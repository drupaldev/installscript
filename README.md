This is the Script file for installing Drupal on your Ubuntu 16.04 system.

To Run the install.sh file, first you have to make it as executable.
To make install.sh file as executable run the following command on your terminal.

$ chmod +x install.sh

After making install.sh as executable, change to root user by executing the following command.

$ sudo -s

Now run the install.sh file as follows.

# ./install.sh

Once all installation is done, run the following command.

# source $HOME/.bashrc

then, go to iitbombayx directory.

# cd /var/www/html/iitbombayx/

Now run the following commands.

# drush8 cr
# drush8 upwd --password="admin" admin

Now open your browser and type.

#    localhost/iitbombayx/user

Now type user name and password.

# Username : admin
# Password : admin

Done!!
