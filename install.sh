#Installation of Drupal using IITBombayX theme.
#Created on 05/07/2020 by Drupal Team I.I.T Bombay.

echo "Downloading packages.."
sudo -S <<< $1 apt-get update
sudo -S <<< $1 apt-get install -y debconf
sudo -S <<< $1 debconf-set-selections <<< "mysql-server mysql-server/root_password password root"
sudo -S <<< $1 debconf-set-selections <<< "mysql-server mysql-server/root_password_again password root"
sudo -S <<< $1 apt-get update
sudo -S <<< $1 apt-get install -y mysql-server

#Server-side installtion and configuration starts here.
echo "Installing packages.."

#Following command will install Apache2, MysqlServer ,Php7.0, and other php packages required for Drupal.
sudo -S <<< $1 apt-get --yes install apache2
sudo -S <<< $1 apt-get --yes install curl
sudo -S <<< $1 apt-get --yes install php7.0
sudo -S <<< $1 apt-get --yes install php7.0-mysql php7.0-xml
sudo -S <<< $1 apt-get --yes install php7.0-gd php7.0-mbstring
sudo -S <<< $1 apt-get --yes install libapache2-mod-php snmp
sudo -S <<< $1 apt-get --yes install libsnmp-dev php7.0-cli
sudo -S <<< $1 apt-get --yes install php7.0-curl php-mcrypt
sudo -S <<< $1 apt-get --yes install php7.0-fpm
sudo -S <<< $1 apt-get --yes install php7.0-zip
sudo -S <<< $1 apt-get --yes install zip
sudo -S <<< $1 apt-get --yes install uzip


#Activating the firewall using Apache Full protection.
echo "Applying Apache Full firewall.."
ufw allow in "Apache Full"

#The following command will replace a line to access index.php from root location.
sudo -S <<< $1 sed -i '/DirectoryIndex index.html index.cgi index.pl index.php index.xhtml index.htm/c\DirectoryIndex index.php index.html index.cgi index.pl index.xhtml index.htm' /etc/apache2/mods-available/dir.conf

#The following command will set allow_url_fopen to Off in php.ini file.
echo " Modifying php.ini file.."
sudo -S <<< $1 sed -i 's/allow_url_fopen\s*=.*/allow_url_fopen=Off/g' /etc/php/7.0/apache2/php.ini
sudo -S <<< $1 sed -i 's/expose_php\s*=.*/expose_php=Off/g' /etc/php/7.0/apache2/php.ini

#Setting apache url rewrite mode.
echo "Setting up the Apache mod_rewrite for Drupal clean urls.."
sudo -S <<< $1 a2enmod rewrite

#This will add the servername name and override all permission in the virtual host.
sudo -S <<< $1 sed -i '/#ServerName www.example.com/c\\t ServerName  localhost' /etc/apache2/sites-available/000-default.conf
sudo -S <<< $1 sed -i '13 a \\t <Directory /var/www/html>' /etc/apache2/sites-available/000-default.conf
sudo -S <<< $1 sed -i '14 a \\t\t AllowOverride All' /etc/apache2/sites-available/000-default.conf
sudo -S <<< $1 sed -i '15 a \\t </Directory>' /etc/apache2/sites-available/000-default.conf

#Restart the apache server.
echo "Restarting the apache server.."
sudo -S <<< $1 service apache2 restart

#Installation of composer.
echo "Installing Composer.."
curl -sS https://getcomposer.org/installer | php
sudo -S <<< $1 mv composer.phar /usr/local/bin/composer

#Installing Drush.
echo "Installing Drush and setting its environment variable.."
sudo composer require drush/drush:8

#Setting Drush8 alias in .Bashrc file.
sudo -S <<< $1 sed -i '94 a \\t alias drush8="$HOME/vendor/drush/drush/drush"' $HOME/.bashrc
source $HOME/.bashrc

#Following command will download and copy the drupal code from Git.
echo "Downloading Drupal from gitlab.."
sudo -S <<< $1 apt-get --yes install git
cd $HOME
mkdir git
cd git
git init
git clone http://gitlab.cse.iitb.ac.in/drupaldev/IITBombayX-Public.git
echo "Extracting the zip file and moving it in drupal folder.."
cd IITBombayX-Public

#Copying the directory to /var/www/html/.
sudo cp -dpR iitbombayx /var/www/html
sudo -S <<< $1 chmod 777 -R /var/www/html/iitbombayx/
sudo -S <<< $1 chown -R www-data:www-data /var/www/html/iitbombayx/

#Exporting MySql Dump.
echo "Creating database.."
mysql -uroot -proot -e "CREATE database iitbombayx;"
echo "Applying database dump.."
mysql -uroot -proot  iitbombayx < $HOME/git/IITBombayX-Public/iitbombayx_16-03-2018.sql
mysql -uroot -proot -e "flush privileges;"

echo "Installation complete.."
echo "Please access the url at your browser eg;http://localhost/iitbombayx"
